var express = require('express');
var router = express.Router();
var Company = require('../models/Company');

router.get('/', async function(req, res) {
    try {
       var companies = await Company.find();
       res.send(companies);
    } catch (err) {
       res.send(err);
    }   
});


router.get('/:id', async function(req, res) {
  try {
     var o_id = req.params.id;
     var company = await Company.findById(o_id);
     res.send(company);
  } catch (err) {
     res.send(err);
  }   
});


router.post('/add', async function(req,res) {
  try {
    var company = new Company({
      name: 'Test Company',
      website: 'Test Website',
      email: 'Test Email'
    })

    await company.save().then(data =>{
      res.send(data);
    }).catch(err =>
      res.send( { message: err})
    );  
  } catch (err) {
      res.send(err)
  }
    
});


router.put('/:id', async function(req,res) {
   try {
    var updatedCompany = await Company.updateOne({_id: req.params.id }, 
       { $set: { name:'HKB Gaming', email: 'hkb-gaming@admin.com', website: 'hkbgaming.com'} })
     res.json(updatedCompany);
   } catch (error) {
     res.json({ message: error })
   }
})


router.delete('/:id', async function(req,res) {
  try {
    var deletedCompany = await Company.remove({_id: req.params.id})
    res.json(deletedCompany);
  } catch (err) {
    res.json({ message: err});
  }   
})

module.exports = router;
