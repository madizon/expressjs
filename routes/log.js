var express = require('express');
var router = express.Router();
var bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
var User = require('../models/User');

router.post('/in', async function(req, res){
 
    var email = "admin@admin.com";
    var password = "password";

    try {
      let user = await User.findOne({ email });

      if (!user)
        return res.status(400).json({
          message: "User Not Exist"
        });

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch)
        return res.status(400).json({
          message: "Invalid Details!"
        });

      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload, "privateKey", { expiresIn: 3600 },
        (err, token) => {
          if (err) throw err;

        res.cookie('jwtToken', token);
        res.send(token);
        }
      );
    } catch (e) {
      console.error(e);
      res.status(500).json({
        message: "Server Error"
      });
    }
  }
);
  
router.post('/out', async function(req, res){
    res.clearCookie('jwtToken');
    res.redirect('/');
});

  module.exports = router;