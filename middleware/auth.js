const jwt = require("jsonwebtoken");
const app = require("../app");

module.exports = function(req, res, next) {
  const token = req.cookies.jwtToken;

  if (!token) return res.status(401).json({ message: "Auth Error" });

  try {
    const decoded = jwt.verify(token, "privateKey");
    req.user = decoded.user;
    next();
  } catch (e) {
    console.error(e);
    res.status(500).send({ message: "Invalid Token" });
  }
};