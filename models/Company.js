var mongoose = require('mongoose');

var CompanySchema = mongoose.Schema({
    name: {
          type: String,
           required: true
    },
    website: String,
    email: String,
    employee_count: Number,
    status: String
},
{
    versionKey: false,
    timestamps: true
})


module.exports = mongoose.model('Company', CompanySchema, 'companies');