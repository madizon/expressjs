var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    name: String,
    email: String,
    password: String
},
{
    versionKey: false,
    timestamps: true
})


module.exports = mongoose.model("User", UserSchema, 'users');